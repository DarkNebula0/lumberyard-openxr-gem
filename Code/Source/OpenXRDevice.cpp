/*
* All or portions of this file Copyright (c) Amazon.com, Inc. or its affiliates or
* its licensors.
*
* For complete copyright and license terms please see the LICENSE at the root of this
* distribution (the "License"). All use of this software is governed by the License,
* or, if provided, by the license below or the license accompanying this file. Do not
* remove or modify any license notices. This file is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*
*/

#include "OpenXR_precompiled.h"
#include "OpenXRDevice.h"
//#include "OpenVRController.h"
#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <IRenderAuxGeom.h>
#include <AzCore/Math/Matrix3x3.h>
#include <AzCore/Math/Matrix4x4.h>
#include <AzCore/Math/MathUtils.h>

//
#define LogMessage(...) CryLogAlways("[HMD][OpenXR] - " __VA_ARGS__);
#define LogError(...) AZ_Error("Script", false, "[HMD][OpenXR] - " __VA_ARGS__);

namespace OpenXR {
	OpenXRDevice::OpenXRDevice()
		: m_pXrInstance(XR_NULL_HANDLE)
		, m_pXrSession(XR_NULL_HANDLE)
		, m_pConfigurationViews(nullptr)
		, m_nViewCount(0)
		, m_pBufferIndex(nullptr)
	{
	}

	void OpenXRDevice::Reflect(AZ::ReflectContext* context)
	{
		if (AZ::SerializeContext* serializeContext = azrtti_cast<AZ::SerializeContext*>(context))
		{
			serializeContext->Class<OpenXRDevice, AZ::Component>()
				->Version(1)
				;

			if (AZ::EditContext* editContext = serializeContext->GetEditContext())
			{
				editContext->Class<OpenXRDevice>(
					"OpenXR Device Manager", "")
					->ClassElement(AZ::Edit::ClassElements::EditorData, "")
					->Attribute(AZ::Edit::Attributes::Category, "VR")
					->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
					;
			}
		}
	}

	void OpenXRDevice::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
	{
		provided.push_back(AZ_CRC("HMDDevice"));
		provided.push_back(AZ_CRC("OpenXRDevice"));
	}
	void OpenXRDevice::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
	{
		incompatible.push_back(AZ_CRC("OpenXRDevice"));
	}

	void OpenXRDevice::Init()
	{
	}

	void OpenXRDevice::Activate()
	{
		AZ::VR::HMDInitRequestBus::Handler::BusConnect();
	}

	void OpenXRDevice::Deactivate()
	{
		AZ::VR::HMDInitRequestBus::Handler::BusDisconnect();
	}

	bool OpenXRDevice::AttemptInit()
	{
		LogMessage("Attempting to initialize OpenXR SDK");
		this->prepareXrInstance();
		return false;
	}
	void OpenXRDevice::Shutdown()
	{
		// TODO Close instance and session
		this->m_pXrInstance = XR_NULL_HANDLE;
		this->m_pXrSession = XR_NULL_HANDLE;
		if (this->m_pConfigurationViews) {
			free(this->m_pConfigurationViews);
		}

		if (this->m_pBufferIndex) {
			delete[] m_pBufferIndex;
		}
	}

	AZ::VR::HMDInitBus::HMDInitPriority OpenXRDevice::GetInitPriority() const
	{
		return AZ::VR::HMDInitBus::HMDInitPriority();
	}

	bool OpenXRDevice::prepareXrInstance()
	{
		uint32_t nExtensionCount = 0;

		XrResult eResult = XrResult::XR_RESULT_MAX_ENUM;

		if (!XR_SUCCEEDED(eResult = xrEnumerateInstanceExtensionProperties(nullptr, 0, &nExtensionCount, nullptr))) {
			// Will be XR_ERROR_RUNTIME_UNAVAILABLE in future
			// https://github.com/KhronosGroup/OpenXR-SDK-Source/pull/177
			if (eResult == XrResult::XR_ERROR_INSTANCE_LOST) {
				LogError("VR-Runtime not found", static_cast<int>(eResult));
				return false;
			}
			LogError("xrEnumerateInstanceExtensionProperties error code: %d", static_cast<int>(eResult));
			return false;
		}

		std::vector<XrExtensionProperties> vector;
		vector.resize(nExtensionCount);
		for (uint32_t i = 0; i < nExtensionCount; ++i) {
			auto& extenionProperties = vector[i];
			extenionProperties = XrExtensionProperties{ XR_TYPE_EXTENSION_PROPERTIES, nullptr };
		}

		eResult = xrEnumerateInstanceExtensionProperties(nullptr, nExtensionCount, &nExtensionCount, vector.data());
		if (!XR_SUCCEEDED(eResult)) {
			LogError("xrEnumerateInstanceExtensionProperties error code: %d", static_cast<int>(eResult));
			return false;
		}

		std::unordered_map<std::string, XrExtensionProperties> aoDiscoveredExtensions;
		for (const auto& extensionProperties : vector) {
			AZ_Printf("Script", "Extenstion: %s", extensionProperties.extensionName);
			aoDiscoveredExtensions.insert({ extensionProperties.extensionName, extensionProperties });
		}

		char* aszExtensions[] = {
			"XR_KHR_D3D11_enable", // Use Direct3D11 for rendering
			"XR_EXT_debug_utils",  // Debug utils for extra info
		};

		const char* pExtensionName = "XR_KHR_D3D11_enable";

		for (auto pExtension : aszExtensions) {
			if (aoDiscoveredExtensions.find(pExtension) == aoDiscoveredExtensions.end()) {
				LogError("Runtime does not support %s extension!", pExtension);
				return false;
			}
		}

		XrInstanceCreateInfo grInstanceCreateInfo;
		grInstanceCreateInfo.type = XR_TYPE_INSTANCE_CREATE_INFO;
		grInstanceCreateInfo.next = NULL;
		grInstanceCreateInfo.createFlags = 0;
		grInstanceCreateInfo.enabledApiLayerCount = 0;
		grInstanceCreateInfo.enabledApiLayerNames = NULL;
		grInstanceCreateInfo.createFlags = 0;
		grInstanceCreateInfo.enabledExtensionCount = sizeof(aszExtensions) / sizeof(char*);
		grInstanceCreateInfo.enabledExtensionNames = aszExtensions;

		strcpy_s(grInstanceCreateInfo.applicationInfo.applicationName, "OpenXRPlugin"); // TODO: Replace
		strcpy_s(grInstanceCreateInfo.applicationInfo.engineName, "Lumberyard");
		grInstanceCreateInfo.applicationInfo.apiVersion = 1;
		grInstanceCreateInfo.applicationInfo.engineVersion = 0;
		grInstanceCreateInfo.applicationInfo.apiVersion = XR_CURRENT_API_VERSION;

		// Create instance
		if (!XR_SUCCEEDED(eResult = xrCreateInstance(&grInstanceCreateInfo, &this->m_pXrInstance))) {
			LogError("xrCreateInstance error code: %d", static_cast<int>(eResult));
			return false;
		}

		XrSystemGetInfo grSystemGetInfo;
		memset(&grSystemGetInfo, 0, sizeof(grSystemGetInfo));
		grSystemGetInfo.type = XR_TYPE_SYSTEM_GET_INFO;
		grSystemGetInfo.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

		if (!XR_SUCCEEDED(eResult = xrGetSystem(this->m_pXrInstance, &grSystemGetInfo, &this->m_oSystemId))) {
			LogError("xrGetSystem error code: %d", static_cast<int>(eResult));
			return false;
		}

		this->m_grSystemProperies.type = XR_TYPE_SYSTEM_PROPERTIES;
		this->m_grSystemProperies.next = NULL;

		if (!XR_SUCCEEDED(eResult = xrGetSystemProperties(this->m_pXrInstance, this->m_oSystemId, &this->m_grSystemProperies))) {
			LogError("xrGetSystemProperties error code: %d", static_cast<int>(eResult));
			return false;
		}

		if (!this->loadExtensionsMethods()) {
			return false;
		}

		// TODO: Print only in debug
		this->printDebugInfos();

		constexpr XrViewConfigurationType eViewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;

		if (!this->isViewConfigSupported(eViewConfigurationType)) {
			LogError("XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO is not supported!");
			return false;
		}
		uint32_t nViewsCount;

		if (!XR_SUCCEEDED(eResult = xrEnumerateViewConfigurationViews(this->m_pXrInstance, this->m_oSystemId, eViewConfigurationType, 0, &nViewsCount, NULL))) {
			LogError("xrEnumerateViewConfigurationViews error code: %d", static_cast<int>(eResult));
			return false;
		}

		this->m_pConfigurationViews = new XrViewConfigurationView[nViewsCount];

		for (uint32_t i = 0; i < nViewsCount; i++) {
			this->m_pConfigurationViews[i].type = XR_TYPE_VIEW_CONFIGURATION_VIEW;
			this->m_pConfigurationViews[i].next = NULL;
		}

		if (!XR_SUCCEEDED(eResult = xrEnumerateViewConfigurationViews(this->m_pXrInstance, this->m_oSystemId, eViewConfigurationType, nViewsCount, &nViewsCount, this->m_pConfigurationViews))) {
			LogError("xrEnumerateViewConfigurationViews error code: %d", static_cast<int>(eResult));
			return false;
		}

		this->m_pBufferIndex = new uint32_t[nViewsCount];
		this->m_nViewCount = nViewsCount;

		return true;
	}

	bool OpenXRDevice::isViewConfigSupported(const XrViewConfigurationType i_eType)
	{
		XrResult eResult;
		uint32_t nConfigurationCount = 0;

		if (!XR_SUCCEEDED(eResult = xrEnumerateViewConfigurations(this->m_pXrInstance, this->m_oSystemId, 0, &nConfigurationCount, NULL))) {
			LogError("xrEnumerateViewConfigurations error code: %d", static_cast<int>(eResult));
			return false;
		}

		std::vector<XrViewConfigurationType> vector;
		vector.resize(nConfigurationCount);

		if (!XR_SUCCEEDED(eResult = xrEnumerateViewConfigurations(this->m_pXrInstance, this->m_oSystemId, nConfigurationCount, &nConfigurationCount, vector.data()))) {
			LogError("xrEnumerateViewConfigurations error code: %d", static_cast<int>(eResult));
			return false;
		}

		for (XrViewConfigurationType& eConfigType : vector) {
			if (eConfigType == i_eType) {
				return true;
			}
		}

		return false;
	}

	bool OpenXRDevice::loadExtensionsMethods()
	{
		XrResult eResult;

		if (!XR_SUCCEEDED(eResult = xrGetInstanceProcAddr(this->m_pXrInstance, "xrCreateDebugUtilsMessengerEXT", reinterpret_cast<PFN_xrVoidFunction*>(&this->m_extXrCreateDebugUtilsMessengerEXT)))) {
			LogError("xrGetInstanceProcAddr error code: %d", static_cast<int>(eResult));
			return false;
		}

		if (!XR_SUCCEEDED(eResult = xrGetInstanceProcAddr(this->m_pXrInstance, "xrDestroyDebugUtilsMessengerEXT", reinterpret_cast<PFN_xrVoidFunction*>(&this->m_extXrDestroyDebugUtilsMessengerEXT)))) {
			LogError("xrGetInstanceProcAddr error code: %d", static_cast<int>(eResult));
			return false;
		}

		if (!XR_SUCCEEDED(eResult = xrGetInstanceProcAddr(this->m_pXrInstance, "xrGetD3D11GraphicsRequirementsKHR", reinterpret_cast<PFN_xrVoidFunction*>(&this->m_extXrGetD3D11GraphicsRequirementsKHR)))) {
			LogError("xrGetInstanceProcAddr error code: %d", static_cast<int>(eResult));
			return false;
		}

		return true;
	}

	bool OpenXRDevice::createSwapchainForViewPort()
	{
		if (!this->m_pConfigurationViews || this->m_nViewCount == 0) {
			return false;
		}

		////XrResult eResult;

		//for (uint32_t i = 0; i < this->m_nViewCount; i++) {
		//	XrViewConfigurationView& grView = this->m_pConfigurationViews[i];
		//	XrSwapchainCreateInfo grSwapchainCreateInfo = { XR_TYPE_SWAPCHAIN_CREATE_INFO };
		//	XrSwapchain	pSwapchainHandle;

		//	grSwapchainCreateInfo.arraySize = 1;
		//	grSwapchainCreateInfo.mipCount = 1;
		//	grSwapchainCreateInfo.faceCount = 1;
		//	grSwapchainCreateInfo.format = DXGI_FORMAT_R8G8B8A8_UNORM;
		//	grSwapchainCreateInfo.width = grView.recommendedImageRectWidth;
		//	grSwapchainCreateInfo.height = grView.recommendedImageRectHeight;
		//	grSwapchainCreateInfo.sampleCount = grView.recommendedSwapchainSampleCount;
		//	grSwapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_SAMPLED_BIT | XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;

		//	/*if (!XR_SUCCEEDED(eResult = xrCreateSwapchain(this->m_p)) {
		//		LogError("xrEnumerateViewConfigurationViews error code: %d", static_cast<int>(eResult));
		//		return false;
		//	}*/
		//}

		return false;
	}

	void OpenXRDevice::printDebugInfos()
	{
		static XrDebugUtilsMessengerEXT xr_debug = {};
		// Set up a really verbose debug log! Great for dev, but turn this off or
		// down for final builds. WMR doesn't produce much output here, but it
		// may be more useful for other runtimes?
		// Here's some extra information about the message types and severities:
		// https://www.khronos.org/registry/OpenXR/specs/1.0/html/xrspec.html#debug-message-categorization
		XrDebugUtilsMessengerCreateInfoEXT debug_info = { XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT };
		debug_info.messageTypes =
			XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
			XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
			XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT |
			XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT;
		debug_info.messageSeverities =
			XR_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
			XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
			XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
			XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		debug_info.userCallback = [](XrDebugUtilsMessageSeverityFlagsEXT severity, XrDebugUtilsMessageTypeFlagsEXT types, const XrDebugUtilsMessengerCallbackDataEXT* msg, void* user_data) {
			// Print the debug message we got! There's a bunch more info we could
			// add here too, but this is a pretty good start, and you can always
			// add a breakpoint this line!
			LogMessage(" % s: % s", msg->functionName, msg->message);

			// Returning XR_TRUE here will force the calling function to fail
			return (XrBool32)XR_FALSE;
		};

		if (this->m_extXrCreateDebugUtilsMessengerEXT)
			this->m_extXrCreateDebugUtilsMessengerEXT(this->m_pXrInstance, &debug_info, &xr_debug);
	}
}